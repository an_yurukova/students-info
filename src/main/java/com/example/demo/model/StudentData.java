package com.example.demo.model;

import java.util.List;

public class StudentData {

  private Long id;
  private String firstName;
  private String lastName;
  private int year;
  private double grade;
  private boolean ongoing;
  private List<String> courseNames;
}
