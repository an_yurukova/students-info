package com.example.demo.service.impl;

import com.example.demo.entity.SignedCourse;
import com.example.demo.entity.Student;
import com.example.demo.repository.StudentRepository;
import com.example.demo.service.StudentService;
import com.example.demo.web.StudentsFilter;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {
  private final StudentRepository studentRepository;

  @Override
  public Set<Student> getAllStudents(StudentsFilter studentsFilter) {

    StudentSpecifications studentSpecifications = new StudentSpecifications(studentsFilter);

    return new HashSet<Student>(studentRepository.findAll(studentSpecifications.getComposite()));
  }

  @RequiredArgsConstructor
  public class StudentSpecifications {

    private final StudentsFilter studentsFilter;

    private Specification<Student> getComposite() {
      return (root, query, criteriaBuilder) -> {
        Join<Student, SignedCourse> studentCoursesJoin = root.join("courses");

        Predicate yearPredicate =
            studentsFilter.getYear().isEmpty()
                ? criteriaBuilder.conjunction()
                : root.get("year").in(studentsFilter.getYear());
        Predicate courseNamePredicate =
            studentsFilter.getCourseName().isEmpty()
                ? criteriaBuilder.conjunction()
                : studentCoursesJoin.get("course").get("name").in(studentsFilter.getCourseName());
        Predicate aboveGradePredicate =
            studentsFilter.getAboveGrade() == null
                ? criteriaBuilder.conjunction()
                : criteriaBuilder.greaterThan(
                    studentCoursesJoin.get("grade"), studentsFilter.getAboveGrade());

        return criteriaBuilder.and(
            criteriaBuilder.and(courseNamePredicate, aboveGradePredicate), yearPredicate);
      };
    }
  }
}
