package com.example.demo.service;

import com.example.demo.entity.Student;
import com.example.demo.web.StudentsFilter;
import java.util.Set;

public interface StudentService {

  Set<Student> getAllStudents(StudentsFilter studentsFilter);
}
