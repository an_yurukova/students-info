package com.example.demo.web;

import java.util.Collections;
import java.util.Set;
import lombok.Data;

@Data
public class StudentsFilter {
  private Set<Integer> year = Collections.emptySet();
  private Set<String> courseName = Collections.emptySet();
  Double aboveGrade;
}
