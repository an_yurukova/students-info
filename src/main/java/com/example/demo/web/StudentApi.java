package com.example.demo.web;

import com.example.demo.entity.Student;
import com.example.demo.service.StudentService;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentApi {

  private final StudentService studentService;

  @GetMapping
  public Set<Student> getAllStudents(StudentsFilter studentsFilter) {

    return studentService.getAllStudents(studentsFilter);
  }
}
