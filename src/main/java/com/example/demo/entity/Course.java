package com.example.demo.entity;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.Data;

@Entity
@Data
public class Course {
  @Id @GeneratedValue private Long id;

  @Column(nullable = false)
  private String name;

  @OneToMany(mappedBy = "course")
  private Set<SignedCourse> signedCourses;
}
