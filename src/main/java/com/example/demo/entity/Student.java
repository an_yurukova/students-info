package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.Data;

@Entity
@Data
public class Student {

  @Id @GeneratedValue private Long id;

  @Column(nullable = false)
  private String name;

  private int year;

  @OneToMany(mappedBy = "student")
  @JsonIgnore
  private Set<SignedCourse> courses;

  public Set<SignedCourse> getCourses() {
    return courses;
  }
}
