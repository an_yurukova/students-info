package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class SignedCourse {
  @Id @GeneratedValue private Long id;

  @Column(nullable = false)
  private double grade;

  @Column(nullable = false)
  private boolean ongoing;

  @ManyToOne
  @JoinColumn(name = "STUDENT_ID", referencedColumnName = "ID")
  private Student student;

  @ManyToOne
  @JoinColumn(name = "COURSE_ID", referencedColumnName = "ID")
  private Course course;
}
